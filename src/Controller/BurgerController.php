<?php

namespace App\Controller;

use App\Entity\Burger;
use App\Form\BurgerType;
use App\Repository\BurgerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/burger")
 */
class BurgerController extends AbstractController
{
    /**
     * @Route("/", name="burger_index", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('burger/index.html.twig');
    }

    /**
     * @Route("/list", name="burger_list", methods={"GET"})
     * @param BurgerRepository $burgerRepository
     * @param Request $request
     * @return Response
     */
    public function list(BurgerRepository $burgerRepository, Request $request): Response
    {
        $burgers = $burgerRepository->findAll();

        // Construction du JSON attendu par le Datatable
        $json_output = array(
            'draw' => $request->get('draw', 0), // Il faut conserver la valeur du paramètre "draw" reçu dans REQUEST
            'recordsTotal' => count($burgers),
            'recordsFiltered' => count($burgers),
            'data' => array()
        );

        foreach ($burgers as $burger) {

            $link_edit = $this->generateUrl('burger_edit', ['id' => $burger->getId()]);
            $link_show = $this->generateUrl('burger_show', ['id' => $burger->getId()]);

            $aRow = array(
                'id' => $burger->getId(),
                'name' => $burger->getName(),
                'desc' => $burger->getDescription(),
                'price' => $burger->getPrice(),
                'supp_double' => $burger->getSuppDouble(),
                'actions' => "<a href='{$link_edit}'>Modifier</a> | <a href='{$link_show}'>Détails</a>"
            );
            $json_output['data'][] = $aRow;
        }

        return new JsonResponse($json_output);
    }

    /**
     * @Route("/new", name="burger_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $burger = new Burger();
        $form = $this->createForm(BurgerType::class, $burger);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($burger);
            $entityManager->flush();

            return $this->redirectToRoute('burger_index');
        }

        return $this->render('burger/new.html.twig', [
            'burger' => $burger,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="burger_show", methods={"GET"})
     * @param Burger $burger
     * @return Response
     */
    public function show(Burger $burger): Response
    {
        return $this->render('burger/show.html.twig', [
            'burger' => $burger,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="burger_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Burger $burger
     * @return Response
     */
    public function edit(Request $request, Burger $burger): Response
    {
        $form = $this->createForm(BurgerType::class, $burger);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('burger_index');
        }

        return $this->render('burger/edit.html.twig', [
            'burger' => $burger,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="burger_delete", methods={"DELETE"})
     * @param Request $request
     * @param Burger $burger
     * @return Response
     */
    public function delete(Request $request, Burger $burger): Response
    {
        if ($this->isCsrfTokenValid('delete'.$burger->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($burger);
            $entityManager->flush();
        }

        return $this->redirectToRoute('burger_index');
    }
}
